﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesHierarchyT1P3
{
    public class Persoana
    
        {
            public String nume { get; set; }
            public String prenume { get; set; }
            public String adresa { get; set; }

            public Persoana(String num, String prenum, String adr)
            {
                nume = num;
                prenume = prenum;
                adresa = adr;
            }

            public override string ToString()
            {
                return nume + "  " + prenume + "   " + adresa;


            }
        
    }
}
